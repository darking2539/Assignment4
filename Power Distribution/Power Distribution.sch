EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:modules
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:Power_Management
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:zetex
LIBS:Zilog
LIBS:arduino_shieldsNCL
LIBS:auv
LIBS:HG_74xx
LIBS:HG_BMS
LIBS:HG_device
LIBS:HG_regulators
LIBS:Power Distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Distribution Board"
Date "2018-06-12"
Rev "ABossZz(Intern)"
Comp "HiveGround Co., Ltd. "
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L i6A24014A033V-001-R U1
U 1 1 5B1F3F59
P 1800 3400
F 0 "U1" H 1400 3550 60  0000 C CNN
F 1 "i6A24014A033V-001-R" H 1850 2650 60  0000 C CNN
F 2 "HG_Devices:DOSA_SixteenthBrick" H 2000 3700 60  0001 C CNN
F 3 "http://www.us.tdk-lambda.com/ftp/specs/i6ap_datasheet.pdf" H 2950 3600 60  0001 C CNN
	1    1800 3400
	1    0    0    -1  
$EndComp
Text Notes 1450 3200 0    60   ~ 0
Regulator1_12V(14A)
$Comp
L i6A24014A033V-001-R U2
U 1 1 5B1F4088
P 1800 4950
F 0 "U2" H 1400 5100 60  0000 C CNN
F 1 "i6A24014A033V-001-R" H 1850 4200 60  0000 C CNN
F 2 "HG_Devices:DOSA_SixteenthBrick" H 2000 5250 60  0001 C CNN
F 3 "http://www.us.tdk-lambda.com/ftp/specs/i6ap_datasheet.pdf" H 2950 5150 60  0001 C CNN
	1    1800 4950
	1    0    0    -1  
$EndComp
Text Notes 1450 4750 0    60   ~ 0
Regulator2_18V(14A)\n
$Comp
L i6A24014A033V-001-R U3
U 1 1 5B1F4162
P 1800 6300
F 0 "U3" H 1400 6450 60  0000 C CNN
F 1 " I6A4W020A033V-001-R" H 1850 5550 60  0000 C CNN
F 2 "HG_Devices:DOSA_SixteenthBrick" H 2000 6600 60  0001 C CNN
F 3 "" H 2950 6500 60  0001 C CNN
F 4 "-" H 1800 6300 60  0001 C CNN "partno"
	1    1800 6300
	1    0    0    -1  
$EndComp
Text Notes 1450 6100 0    60   ~ 0
Regulator3_12V(20A)\n
$Comp
L R R1
U 1 1 5B1F43BE
P 2700 3650
F 0 "R1" V 2780 3650 50  0000 C CNN
F 1 "1.4k" V 2700 3650 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" V 2630 3650 50  0001 C CNN
F 3 "" H 2700 3650 50  0001 C CNN
F 4 "MC0603SAF1401T5E" H 2700 3650 60  0001 C CNN "partno"
	1    2700 3650
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5B1F44A7
P 2700 5200
F 0 "R2" V 2780 5200 50  0000 C CNN
F 1 "750" V 2700 5200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603" V 2630 5200 50  0001 C CNN
F 3 "" H 2700 5200 50  0001 C CNN
F 4 "WR06X7500FTL" H 2700 5200 60  0001 C CNN "partno"
	1    2700 5200
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5B1F458B
P 2700 6550
F 0 "R3" V 2780 6550 50  0000 C CNN
F 1 "1.8k" V 2700 6550 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 2630 6550 50  0001 C CNN
F 3 "" H 2700 6550 50  0001 C CNN
F 4 "RT0402BRD071K8L" H 2700 6550 60  0001 C CNN "partno"
	1    2700 6550
	0    1    1    0   
$EndComp
Text GLabel 3000 3450 2    60   Output ~ 0
12V_3
Text GLabel 2950 5000 2    60   Output ~ 0
18V_1
Text GLabel 2950 6350 2    60   Output ~ 0
12V_1
Text Notes 550  2950 0    60   ~ 0
RegulatorGroup3\n
Text Notes 550  4500 0    60   ~ 0
RegulatorGroup1\n
Text Notes 3700 650  0    60   ~ 0
CP-6.5mm(2pins)
Text Notes 3700 6500 0    60   ~ 0
Ultra-Fit
$Comp
L Conn_01x02 J16
U 1 1 5B20BC64
P 4750 6800
F 0 "J16" H 4750 6900 50  0000 C CNN
F 1 "Arduino [nano] (env)" H 4750 6600 50  0000 C CNN
F 2 "PCBlib:Ultrafit_2pins" H 4750 6800 50  0001 C CNN
F 3 "" H 4750 6800 50  0001 C CNN
F 4 "1722872102" H 4750 6800 60  0001 C CNN "partno"
	1    4750 6800
	1    0    0    -1  
$EndComp
Text Notes 7100 2650 0    60   ~ 0
Mega-Fit
Text Notes 600  650  0    60   ~ 0
Input\n
$Comp
L Conn_01x02 J4
U 1 1 5B20FAD0
P 4750 900
F 0 "J4" H 4750 1000 50  0000 C CNN
F 1 "SG-95D-08 [8 Ports]" H 4750 700 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 900 50  0001 C CNN
F 3 "" H 4750 900 50  0001 C CNN
F 4 "1510482200" H 4750 900 60  0001 C CNN "partno"
	1    4750 900 
	1    0    0    -1  
$EndComp
Text Notes 600  1150 0    60   ~ 0
Group1_30A
Text Notes 600  2100 0    60   ~ 0
Group3_60A
$Comp
L Conn_02x01 J18
U 1 1 5B214B4A
P 8750 2850
F 0 "J18" H 8800 2950 50  0000 C CNN
F 1 "Servo" H 8800 2750 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x01x5.70mm_Straight" H 8750 2850 50  0001 C CNN
F 3 "" H 8750 2850 50  0001 C CNN
F 4 "768290002" H 8750 2850 60  0001 C CNN "partno"
	1    8750 2850
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x01 J19
U 1 1 5B214E30
P 10650 2850
F 0 "J19" H 10700 2950 50  0000 C CNN
F 1 "Spare servo" H 10700 2750 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x01x5.70mm_Straight" H 10650 2850 50  0001 C CNN
F 3 "" H 10650 2850 50  0001 C CNN
F 4 "768290002" H 10650 2850 60  0001 C CNN "partno"
	1    10650 2850
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x03_Counter_Clockwise J24
U 1 1 5B21987F
P 1800 1150
F 0 "J24" H 1850 1350 50  0000 C CNN
F 1 "Input Group1" H 1850 950 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x03x5.70mm_Straight" H 1800 1150 50  0001 C CNN
F 3 "" H 1800 1150 50  0001 C CNN
F 4 "768290006" H 1800 1150 60  0001 C CNN "partno"
	1    1800 1150
	-1   0    0    1   
$EndComp
$Comp
L Conn_02x03_Counter_Clockwise J25
U 1 1 5B219B6C
P 1800 2000
F 0 "J25" H 1850 2200 50  0000 C CNN
F 1 "Input Group3" H 1850 1800 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x03x5.70mm_Straight" H 1800 2000 50  0001 C CNN
F 3 "" H 1800 2000 50  0001 C CNN
F 4 "768290006" H 1800 2000 60  0001 C CNN "partno"
	1    1800 2000
	-1   0    0    1   
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J20
U 1 1 5B21A159
P 8400 3700
F 0 "J20" H 8450 3800 50  0000 C CNN
F 1 "Motor 1" H 8450 3500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x02x5.70mm_Straight" H 8400 3700 50  0001 C CNN
F 3 "" H 8400 3700 50  0001 C CNN
F 4 "768290004" H 8400 3700 60  0001 C CNN "partno"
	1    8400 3700
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J21
U 1 1 5B21A3C9
P 10150 3700
F 0 "J21" H 10200 3800 50  0000 C CNN
F 1 "Motor 2" H 10200 3500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x02x5.70mm_Straight" H 10150 3700 50  0001 C CNN
F 3 "" H 10150 3700 50  0001 C CNN
F 4 "768290004" H 10150 3700 60  0001 C CNN "partno"
	1    10150 3700
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J22
U 1 1 5B21A446
P 8400 4700
F 0 "J22" H 8450 4800 50  0000 C CNN
F 1 "Motor 3" H 8450 4500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x02x5.70mm_Straight" H 8400 4700 50  0001 C CNN
F 3 "" H 8400 4700 50  0001 C CNN
F 4 "768290004" H 8400 4700 60  0001 C CNN "partno"
	1    8400 4700
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J23
U 1 1 5B21A4B8
P 10150 4700
F 0 "J23" H 10200 4800 50  0000 C CNN
F 1 "Spare Motor" H 10200 4500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MegaFit_2x02x5.70mm_Straight" H 10150 4700 50  0001 C CNN
F 3 "" H 10150 4700 50  0001 C CNN
F 4 "768290004" H 10150 4700 60  0001 C CNN "partno"
	1    10150 4700
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J1
U 1 1 5B2210B2
P 8000 850
F 0 "J1" H 8050 950 50  0000 C CNN
F 1 "NUC(master)" H 8050 650 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_4pins" H 8000 850 50  0001 C CNN
F 3 "" H 8000 850 50  0001 C CNN
F 4 "1510481401" H 8000 850 60  0001 C CNN "partno"
	1    8000 850 
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J17
U 1 1 5B22340B
P 6200 6800
F 0 "J17" H 6200 6900 50  0000 C CNN
F 1 "Arduino [nano] (light)" H 6200 6600 50  0000 C CNN
F 2 "PCBlib:Ultrafit_2pins" H 6200 6800 50  0001 C CNN
F 3 "" H 6200 6800 50  0001 C CNN
F 4 "1722872102" H 6200 6800 60  0001 C CNN "partno"
	1    6200 6800
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J2
U 1 1 5B223B22
P 9700 850
F 0 "J2" H 9750 950 50  0000 C CNN
F 1 "NUC(video)" H 9750 650 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_4pins" H 9700 850 50  0001 C CNN
F 3 "" H 9700 850 50  0001 C CNN
F 4 "1510481401" H 9700 850 60  0001 C CNN "partno"
	1    9700 850 
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x02_Counter_Clockwise J3
U 1 1 5B223B82
P 8000 1750
F 0 "J3" H 8050 1850 50  0000 C CNN
F 1 "Spare 18V" H 8050 1550 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_4pins" H 8000 1750 50  0001 C CNN
F 3 "" H 8000 1750 50  0001 C CNN
F 4 "1510481411" H 8000 1750 60  0001 C CNN "partno"
	1    8000 1750
	1    0    0    -1  
$EndComp
Text GLabel 4150 900  0    60   Input ~ 0
12V_1
Text GLabel 7500 900  0    60   Input ~ 0
18V_1
Text GLabel 9150 900  0    60   Input ~ 0
18V_1
Text GLabel 7500 1800 0    60   Input ~ 0
18V_1
Text GLabel 5650 6800 0    60   Input ~ 0
25.2V_1
Text GLabel 4200 6800 0    60   Input ~ 0
25.2V_1
Text GLabel 2650 1150 2    60   Output ~ 0
25.2V_1
Text GLabel 2650 2000 2    60   Output ~ 0
25.2V_3
$Comp
L C C25
U 1 1 5B23A448
P 2400 2300
F 0 "C25" H 2425 2400 50  0000 L CNN
F 1 "100uF" H 2425 2200 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 2438 2150 50  0001 C CNN
F 3 "" H 2400 2300 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 2400 2300 60  0001 C CNN "partno"
	1    2400 2300
	1    0    0    -1  
$EndComp
Text GLabel 1100 3450 0    60   Input ~ 0
25.2V_3
Text GLabel 1100 5000 0    60   Input ~ 0
25.2V_1
Text GLabel 1100 6350 0    60   Input ~ 0
25.2V_1
$Comp
L C C16
U 1 1 5B28693C
P 4300 7000
F 0 "C16" H 4325 7100 50  0000 L CNN
F 1 "47uF" H 4000 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 4338 6850 50  0001 C CNN
F 3 "" H 4300 7000 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 4300 7000 60  0001 C CNN "partno"
	1    4300 7000
	1    0    0    -1  
$EndComp
$Comp
L C C17
U 1 1 5B2880F3
P 5750 7000
F 0 "C17" H 5775 7100 50  0000 L CNN
F 1 "47uF" H 5450 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 5788 6850 50  0001 C CNN
F 3 "" H 5750 7000 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 5750 7000 60  0001 C CNN "partno"
	1    5750 7000
	1    0    0    -1  
$EndComp
$Comp
L C C18-1
U 1 1 5B28A587
P 7850 3000
F 0 "C18-1" H 7700 3100 50  0000 L CNN
F 1 "100uF" H 7850 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 7888 2850 50  0001 C CNN
F 3 "" H 7850 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 7850 3000 60  0001 C CNN "partno"
	1    7850 3000
	1    0    0    -1  
$EndComp
$Comp
L C C18-2
U 1 1 5B28A593
P 8100 3000
F 0 "C18-2" H 8000 3100 50  0000 L CNN
F 1 "100uF" H 8125 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 8138 2850 50  0001 C CNN
F 3 "" H 8100 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 8100 3000 60  0001 C CNN "partno"
	1    8100 3000
	1    0    0    -1  
$EndComp
$Comp
L C C18-3
U 1 1 5B28A714
P 8350 3000
F 0 "C18-3" H 8350 3100 50  0000 L CNN
F 1 "100uF" H 8375 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 8388 2850 50  0001 C CNN
F 3 "" H 8350 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 8350 3000 60  0001 C CNN "partno"
	1    8350 3000
	1    0    0    -1  
$EndComp
Text GLabel 7550 2850 0    60   Input ~ 0
12V_3
Text GLabel 9600 2850 0    60   Input ~ 0
12V_3
Text GLabel 7750 3750 0    60   Input ~ 0
25.2V_3
Text GLabel 7750 4750 0    60   Input ~ 0
25.2V_3
Text GLabel 9500 3750 0    60   Input ~ 0
25.2V_3
Text GLabel 9500 4750 0    60   Input ~ 0
25.2V_3
$Comp
L Conn_01x02 J5
U 1 1 5B2AB5EB
P 6350 900
F 0 "J5" H 6350 1000 50  0000 C CNN
F 1 "SG-95D-08 [8 Ports]" H 6350 700 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 900 50  0001 C CNN
F 3 "" H 6350 900 50  0001 C CNN
F 4 "1510482200" H 6350 900 60  0001 C CNN "partno"
	1    6350 900 
	1    0    0    -1  
$EndComp
Text GLabel 5750 900  0    60   Input ~ 0
12V_1
$Comp
L Conn_01x02 J6
U 1 1 5B2AB913
P 4750 1850
F 0 "J6" H 4750 1950 50  0000 C CNN
F 1 "USB3 Hub" H 4750 1650 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 1850 50  0001 C CNN
F 3 "" H 4750 1850 50  0001 C CNN
F 4 "1510482203" H 4750 1850 60  0001 C CNN "partno"
	1    4750 1850
	1    0    0    -1  
$EndComp
Text GLabel 4150 1850 0    60   Input ~ 0
12V_1
$Comp
L C C6
U 1 1 5B2AB91B
P 4250 2150
F 0 "C6" H 4100 2250 50  0000 L CNN
F 1 "100uF" H 4275 2050 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 4288 2000 50  0001 C CNN
F 3 "" H 4250 2150 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 4250 2150 60  0001 C CNN "partno"
	1    4250 2150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J7
U 1 1 5B2AB92F
P 6350 1850
F 0 "J7" H 6350 1950 50  0000 C CNN
F 1 "Audio Amp" H 6350 1650 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 1850 50  0001 C CNN
F 3 "" H 6350 1850 50  0001 C CNN
F 4 "1510482204" H 6350 1850 60  0001 C CNN "partno"
	1    6350 1850
	1    0    0    -1  
$EndComp
Text GLabel 5750 1850 0    60   Input ~ 0
12V_1
$Comp
L Conn_01x02 J8
U 1 1 5B2ABD73
P 4750 2750
F 0 "J8" H 4750 2850 50  0000 C CNN
F 1 "Hokuyo" H 4750 2550 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 2750 50  0001 C CNN
F 3 "" H 4750 2750 50  0001 C CNN
F 4 "1510482209" H 4750 2750 60  0001 C CNN "partno"
	1    4750 2750
	1    0    0    -1  
$EndComp
Text GLabel 4150 2750 0    60   Input ~ 0
12V_1
$Comp
L Conn_01x02 J9
U 1 1 5B2ABD8F
P 6350 2750
F 0 "J9" H 6350 2850 50  0000 C CNN
F 1 "Camera (top front)" H 6350 2550 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 2750 50  0001 C CNN
F 3 "" H 6350 2750 50  0001 C CNN
F 4 "1510482202" H 6350 2750 60  0001 C CNN "partno"
	1    6350 2750
	1    0    0    -1  
$EndComp
Text GLabel 5750 2750 0    60   Input ~ 0
12V_1
$Comp
L C C9
U 1 1 5B2ABD97
P 5850 3050
F 0 "C9" H 5700 3150 50  0000 L CNN
F 1 "47uF" H 5875 2950 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 5888 2900 50  0001 C CNN
F 3 "" H 5850 3050 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 5850 3050 60  0001 C CNN "partno"
	1    5850 3050
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J10
U 1 1 5B2ABDAB
P 4750 3700
F 0 "J10" H 4750 3800 50  0000 C CNN
F 1 "Camera (top left)" H 4750 3500 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 3700 50  0001 C CNN
F 3 "" H 4750 3700 50  0001 C CNN
F 4 "1510482202" H 4750 3700 60  0001 C CNN "partno"
	1    4750 3700
	1    0    0    -1  
$EndComp
Text GLabel 4150 3700 0    60   Input ~ 0
12V_1
$Comp
L C C10
U 1 1 5B2ABDB3
P 4250 4000
F 0 "C10" H 4100 4100 50  0000 L CNN
F 1 "47uF" H 4275 3900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 4288 3850 50  0001 C CNN
F 3 "" H 4250 4000 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 4250 4000 60  0001 C CNN "partno"
	1    4250 4000
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J11
U 1 1 5B2ABDC7
P 6350 3700
F 0 "J11" H 6350 3800 50  0000 C CNN
F 1 "Camera (top right)" H 6350 3500 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 3700 50  0001 C CNN
F 3 "" H 6350 3700 50  0001 C CNN
F 4 "1510482202" H 6350 3700 60  0001 C CNN "partno"
	1    6350 3700
	1    0    0    -1  
$EndComp
Text GLabel 5750 3700 0    60   Input ~ 0
12V_1
$Comp
L C C11
U 1 1 5B2ABDCF
P 5850 4000
F 0 "C11" H 5700 4100 50  0000 L CNN
F 1 "47uF" H 5875 3900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 5888 3850 50  0001 C CNN
F 3 "" H 5850 4000 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 5850 4000 60  0001 C CNN "partno"
	1    5850 4000
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J12
U 1 1 5B2AC975
P 4750 4600
F 0 "J12" H 4750 4700 50  0000 C CNN
F 1 "Camera (lower left)" H 4750 4400 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 4600 50  0001 C CNN
F 3 "" H 4750 4600 50  0001 C CNN
F 4 "1510482202" H 4750 4600 60  0001 C CNN "partno"
	1    4750 4600
	1    0    0    -1  
$EndComp
Text GLabel 4150 4600 0    60   Input ~ 0
12V_1
$Comp
L C C12
U 1 1 5B2AC97D
P 4250 4900
F 0 "C12" H 4100 5000 50  0000 L CNN
F 1 "47uF" H 4275 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 4288 4750 50  0001 C CNN
F 3 "" H 4250 4900 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 4250 4900 60  0001 C CNN "partno"
	1    4250 4900
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J13
U 1 1 5B2AC991
P 6350 4600
F 0 "J13" H 6350 4700 50  0000 C CNN
F 1 "Camera (lower left)" H 6350 4400 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 4600 50  0001 C CNN
F 3 "" H 6350 4600 50  0001 C CNN
F 4 "1510482202" H 6350 4600 60  0001 C CNN "partno"
	1    6350 4600
	1    0    0    -1  
$EndComp
Text GLabel 5750 4600 0    60   Input ~ 0
12V_1
$Comp
L C C13
U 1 1 5B2AC999
P 5850 4900
F 0 "C13" H 5700 5000 50  0000 L CNN
F 1 "47uF" H 5875 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 5888 4750 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 5850 4900 60  0001 C CNN "partno"
	1    5850 4900
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J14
U 1 1 5B2AC9AD
P 4750 5550
F 0 "J14" H 4750 5650 50  0000 C CNN
F 1 "Arduino [nano] (sonar)" H 4750 5350 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 4750 5550 50  0001 C CNN
F 3 "" H 4750 5550 50  0001 C CNN
F 4 "1510481207" H 4750 5550 60  0001 C CNN "partno"
	1    4750 5550
	1    0    0    -1  
$EndComp
Text GLabel 4150 5550 0    60   Input ~ 0
12V_1
$Comp
L C C14
U 1 1 5B2AC9B5
P 4250 5850
F 0 "C14" H 4100 5950 50  0000 L CNN
F 1 "47uF" H 4275 5750 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 4288 5700 50  0001 C CNN
F 3 "" H 4250 5850 50  0001 C CNN
F 4 "TMK325ABJ476MM-T" H 4250 5850 60  0001 C CNN "partno"
	1    4250 5850
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J15
U 1 1 5B2AC9C9
P 6350 5550
F 0 "J15" H 6350 5650 50  0000 C CNN
F 1 "Spare 12V" H 6350 5350 50  0000 C CNN
F 2 "PCBlib:CP6.5mm_2pins" H 6350 5550 50  0001 C CNN
F 3 "" H 6350 5550 50  0001 C CNN
F 4 "1510481211" H 6350 5550 60  0001 C CNN "partno"
	1    6350 5550
	1    0    0    -1  
$EndComp
Text GLabel 5750 5550 0    60   Input ~ 0
12V_1
Text Notes 7100 650  0    60   ~ 0
CP-6.5mm(4pins)
$Comp
L CP1 C24-1
U 1 1 5B29FE80
P 2250 1450
F 0 "C24-1" H 2150 1600 50  0000 L CNN
F 1 "220uF" H 2275 1350 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 2250 1450 50  0001 C CNN
F 3 "" H 2250 1450 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 2250 1450 60  0001 C CNN "partno"
	1    2250 1450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C24-2
U 1 1 5B2A0199
P 2500 1450
F 0 "C24-2" H 2450 1600 50  0000 L CNN
F 1 "220uF" H 2525 1350 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 2500 1450 50  0001 C CNN
F 3 "" H 2500 1450 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 2500 1450 60  0001 C CNN "partno"
	1    2500 1450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C4
U 1 1 5B2A0D05
P 4250 1200
F 0 "C4" H 4100 1350 50  0000 L CNN
F 1 "220uF" H 4275 1100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 4250 1200 50  0001 C CNN
F 3 "" H 4250 1200 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 4250 1200 60  0001 C CNN "partno"
	1    4250 1200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C5
U 1 1 5B2A27F8
P 5850 1200
F 0 "C5" H 5700 1350 50  0000 L CNN
F 1 "220uF" H 5875 1100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 5850 1200 50  0001 C CNN
F 3 "" H 5850 1200 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 5850 1200 60  0001 C CNN "partno"
	1    5850 1200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C7
U 1 1 5B2A3033
P 5850 2150
F 0 "C7" H 5700 2300 50  0000 L CNN
F 1 "220uF" H 5875 2050 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 5850 2150 50  0001 C CNN
F 3 "" H 5850 2150 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 5850 2150 60  0001 C CNN "partno"
	1    5850 2150
	1    0    0    -1  
$EndComp
$Comp
L CP1 C8
U 1 1 5B2A363C
P 4250 3050
F 0 "C8" H 4100 3200 50  0000 L CNN
F 1 "220uF" H 4275 2950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 4250 3050 50  0001 C CNN
F 3 "" H 4250 3050 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 4250 3050 60  0001 C CNN "partno"
	1    4250 3050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C15
U 1 1 5B2A4C30
P 5850 5850
F 0 "C15" H 5700 6000 50  0000 L CNN
F 1 "220uF" H 5875 5750 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 5850 5850 50  0001 C CNN
F 3 "" H 5850 5850 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 5850 5850 60  0001 C CNN "partno"
	1    5850 5850
	1    0    0    -1  
$EndComp
$Comp
L CP1 C1
U 1 1 5B2A5999
P 7650 1200
F 0 "C1" H 7500 1350 50  0000 L CNN
F 1 "220uF" H 7675 1100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 7650 1200 50  0001 C CNN
F 3 "" H 7650 1200 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 7650 1200 60  0001 C CNN "partno"
	1    7650 1200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C2
U 1 1 5B2A5B12
P 9300 1200
F 0 "C2" H 9150 1350 50  0000 L CNN
F 1 "220uF" H 9325 1100 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 9300 1200 50  0001 C CNN
F 3 "" H 9300 1200 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 9300 1200 60  0001 C CNN "partno"
	1    9300 1200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C20
U 1 1 5B2A62AD
P 7850 4050
F 0 "C20" H 7700 4200 50  0000 L CNN
F 1 "220uF" H 7875 3950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 7850 4050 50  0001 C CNN
F 3 "" H 7850 4050 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 7850 4050 60  0001 C CNN "partno"
	1    7850 4050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C21
U 1 1 5B2A6376
P 9600 4050
F 0 "C21" H 9450 4200 50  0000 L CNN
F 1 "220uF" H 9625 3950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 9600 4050 50  0001 C CNN
F 3 "" H 9600 4050 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 9600 4050 60  0001 C CNN "partno"
	1    9600 4050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C22
U 1 1 5B2A6817
P 7850 5050
F 0 "C22" H 7700 5200 50  0000 L CNN
F 1 "220uF" H 7875 4950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 7850 5050 50  0001 C CNN
F 3 "" H 7850 5050 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 7850 5050 60  0001 C CNN "partno"
	1    7850 5050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C23
U 1 1 5B2A0A92
P 9650 5050
F 0 "C23" H 9500 5200 50  0000 L CNN
F 1 "220uF" H 9675 4950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 9650 5050 50  0001 C CNN
F 3 "" H 9650 5050 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 9650 5050 60  0001 C CNN "partno"
	1    9650 5050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C3
U 1 1 5B2A4FDB
P 7650 2100
F 0 "C3" H 7500 2250 50  0000 L CNN
F 1 "220uF" H 7675 2000 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10" H 7650 2100 50  0001 C CNN
F 3 "" H 7650 2100 50  0001 C CNN
F 4 "UUD1H221MNL1GS" H 7650 2100 60  0001 C CNN "partno"
	1    7650 2100
	1    0    0    -1  
$EndComp
$Comp
L C C19-1
U 1 1 5B2BC2C0
P 9750 3000
F 0 "C19-1" H 9600 3100 50  0000 L CNN
F 1 "100uF" H 9750 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 9788 2850 50  0001 C CNN
F 3 "" H 9750 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 9750 3000 60  0001 C CNN "partno"
	1    9750 3000
	1    0    0    -1  
$EndComp
$Comp
L C C19-2
U 1 1 5B2BC2CD
P 10000 3000
F 0 "C19-2" H 9900 3100 50  0000 L CNN
F 1 "100uF" H 10025 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 10038 2850 50  0001 C CNN
F 3 "" H 10000 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 10000 3000 60  0001 C CNN "partno"
	1    10000 3000
	1    0    0    -1  
$EndComp
$Comp
L C C19-3
U 1 1 5B2BC2DA
P 10250 3000
F 0 "C19-3" H 10200 3100 50  0000 L CNN
F 1 "100uF" H 10275 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 10288 2850 50  0001 C CNN
F 3 "" H 10250 3000 50  0001 C CNN
F 4 "C3216JB1A107M160AC" H 10250 3000 60  0001 C CNN "partno"
	1    10250 3000
	1    0    0    -1  
$EndComp
Text GLabel 2650 1700 2    60   Output ~ 0
GND25.2V_1
Text GLabel 2500 2600 2    60   Output ~ 0
GND25.2V_3
Text GLabel 1150 3850 0    60   Output ~ 0
GND25.2V_3
Text GLabel 2600 3850 2    60   Output ~ 0
GND12V_3
Text GLabel 1150 5400 0    60   Output ~ 0
GND25.2V_1
Text GLabel 2550 5400 2    60   Output ~ 0
GND18V_1
Text GLabel 1150 6750 0    60   Output ~ 0
GND25.2V_1
Text GLabel 2900 6550 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 1550 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 1550 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 2500 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 2500 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 3400 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 3400 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 4350 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 4350 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 5250 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 5250 2    60   Output ~ 0
GND12V_1
Text GLabel 4600 6200 2    60   Output ~ 0
GND12V_1
Text GLabel 6200 6200 2    60   Output ~ 0
GND12V_1
Text GLabel 4500 7350 2    60   Output ~ 0
GND25.2V_1
Text GLabel 5950 7350 2    60   Output ~ 0
GND25.2V_1
Text GLabel 8550 1400 2    60   Output ~ 0
GND18V_1
Text GLabel 10150 1350 2    60   Output ~ 0
GND18V_1
Text GLabel 8550 2300 2    60   Output ~ 0
GND18V_1
Text GLabel 7750 3400 0    60   Output ~ 0
GND12V_3
Text GLabel 9650 3400 0    60   Output ~ 0
GND12V_3
Text GLabel 7800 4350 0    60   Output ~ 0
GND25.2V_3
Text GLabel 9550 4350 0    60   Output ~ 0
GND25.2V_3
Text GLabel 7800 5250 0    60   Output ~ 0
GND25.2V_3
Text GLabel 9450 5250 0    60   Output ~ 0
GND25.2V_3
Text GLabel 2900 3650 2    60   Output ~ 0
GND12V_3
Text GLabel 2900 5200 2    60   Output ~ 0
GND18V_1
Connection ~ 9300 900 
Wire Wire Line
	9300 1050 9300 900 
Connection ~ 7650 900 
Wire Wire Line
	7650 1050 7650 900 
Connection ~ 9500 900 
Wire Wire Line
	9150 900  9500 900 
Wire Notes Line
	550  2750 550  700 
Wire Notes Line
	3700 6550 6700 6550
Wire Notes Line
	550  4300 550  3000
Wire Wire Line
	2450 6350 2950 6350
Wire Wire Line
	2450 5000 2950 5000
Wire Wire Line
	2450 3450 3000 3450
Wire Wire Line
	4150 900  4550 900 
Wire Wire Line
	7500 900  7800 900 
Wire Wire Line
	7800 850  7800 950 
Connection ~ 7800 900 
Wire Wire Line
	7800 1750 7800 1850
Connection ~ 7800 1800
Wire Wire Line
	2000 1050 2100 1050
Wire Wire Line
	2100 1050 2100 1250
Wire Wire Line
	2100 1250 2000 1250
Connection ~ 2100 1150
Wire Wire Line
	2250 1150 2250 1300
Wire Wire Line
	2500 1150 2500 1300
Wire Wire Line
	2000 1150 2650 1150
Connection ~ 2250 1150
Connection ~ 2500 1150
Wire Wire Line
	2000 1900 2100 1900
Wire Wire Line
	2100 1900 2100 2100
Wire Wire Line
	2100 2100 2000 2100
Connection ~ 2100 2000
Wire Wire Line
	2400 2000 2400 2150
Connection ~ 2400 2000
Wire Wire Line
	2000 2000 2650 2000
Wire Wire Line
	1500 1050 1400 1050
Wire Wire Line
	1400 1050 1400 1250
Wire Wire Line
	1400 1250 1500 1250
Wire Wire Line
	1300 1150 1500 1150
Connection ~ 1400 1150
Wire Wire Line
	1300 1150 1300 1700
Wire Wire Line
	1300 2000 1300 2600
Wire Wire Line
	1500 1900 1400 1900
Wire Wire Line
	1400 1900 1400 2100
Wire Wire Line
	1400 2100 1500 2100
Wire Wire Line
	1300 2000 1500 2000
Connection ~ 1400 2000
Wire Wire Line
	1100 3450 1250 3450
Wire Wire Line
	1100 5000 1250 5000
Wire Wire Line
	1100 6350 1250 6350
Wire Wire Line
	8300 850  8350 850 
Wire Wire Line
	8350 850  8350 950 
Wire Wire Line
	8350 950  8300 950 
Connection ~ 8350 900 
Wire Wire Line
	7800 1800 7500 1800
Wire Wire Line
	8300 1750 8350 1750
Wire Wire Line
	8350 1750 8350 1850
Wire Wire Line
	8350 1850 8300 1850
Connection ~ 8350 1800
Wire Wire Line
	9500 850  9500 950 
Wire Wire Line
	10000 850  10050 850 
Wire Wire Line
	10050 850  10050 950 
Wire Wire Line
	10050 950  10000 950 
Wire Wire Line
	10050 900  10100 900 
Connection ~ 10050 900 
Wire Wire Line
	4550 6800 4200 6800
Wire Wire Line
	4300 6850 4300 6800
Connection ~ 4300 6800
Wire Wire Line
	4550 7150 4550 6900
Wire Wire Line
	4300 7150 4550 7150
Connection ~ 4450 7150
Wire Wire Line
	6000 6800 5650 6800
Wire Wire Line
	5750 6850 5750 6800
Connection ~ 5750 6800
Wire Wire Line
	5750 7150 6000 7150
Connection ~ 5900 7150
Wire Wire Line
	6000 7150 6000 6900
Wire Wire Line
	7550 2850 8550 2850
Connection ~ 7850 2850
Connection ~ 8100 2850
Connection ~ 8350 2850
Wire Wire Line
	7850 3900 7850 3750
Wire Wire Line
	8100 3700 8100 3800
Wire Wire Line
	8100 3800 8200 3800
Wire Wire Line
	7750 3750 8100 3750
Connection ~ 8100 3750
Connection ~ 7850 3750
Wire Wire Line
	8750 3700 8750 3800
Wire Wire Line
	8750 3800 8700 3800
Wire Wire Line
	8750 3750 8800 3750
Connection ~ 8750 3750
Wire Wire Line
	7850 4900 7850 4750
Wire Wire Line
	8200 4700 8100 4700
Wire Wire Line
	8100 4700 8100 4800
Wire Wire Line
	8100 4800 8200 4800
Wire Wire Line
	7750 4750 8100 4750
Connection ~ 8100 4750
Connection ~ 7850 4750
Wire Wire Line
	9600 3900 9600 3750
Wire Wire Line
	9850 3700 9850 3800
Wire Wire Line
	9850 3800 9950 3800
Wire Wire Line
	9500 3750 9850 3750
Connection ~ 9850 3750
Connection ~ 9600 3750
Wire Wire Line
	9500 4750 9900 4750
Wire Wire Line
	9950 4700 9900 4700
Wire Wire Line
	9900 4700 9900 4800
Wire Wire Line
	9900 4800 9950 4800
Connection ~ 9900 4750
Wire Wire Line
	8700 4700 8750 4700
Wire Wire Line
	8750 4700 8750 4800
Wire Wire Line
	8750 4800 8700 4800
Wire Wire Line
	8800 4750 8750 4750
Connection ~ 8750 4750
Wire Wire Line
	10500 3700 10500 3800
Wire Wire Line
	10500 3800 10450 3800
Wire Wire Line
	10500 3750 10550 3750
Connection ~ 10500 3750
Wire Wire Line
	10450 4700 10500 4700
Wire Wire Line
	10500 4700 10500 4800
Wire Wire Line
	10500 4800 10450 4800
Wire Wire Line
	10500 4750 10550 4750
Connection ~ 10500 4750
Wire Notes Line
	7100 2700 11100 2700
Wire Notes Line
	550  700  3500 700 
Wire Notes Line
	550  2750 3500 2750
Wire Notes Line
	550  3000 3500 3000
Wire Notes Line
	550  4300 3500 4300
Wire Notes Line
	550  4550 3500 4550
Wire Notes Line
	550  7150 3500 7150
Wire Notes Line
	550  7150 550  4550
Wire Wire Line
	8450 900  8350 900 
Wire Wire Line
	8450 1800 8350 1800
Wire Wire Line
	4250 1050 4250 900 
Connection ~ 4250 900 
Wire Wire Line
	4250 1350 4250 1450
Wire Wire Line
	4250 1450 4550 1450
Wire Wire Line
	4550 1450 4550 1000
Wire Wire Line
	4400 1450 4400 1550
Connection ~ 4400 1450
Wire Wire Line
	5750 900  6150 900 
Wire Wire Line
	5850 1050 5850 900 
Connection ~ 5850 900 
Wire Wire Line
	5850 1350 5850 1450
Wire Wire Line
	5850 1450 6150 1450
Wire Wire Line
	6150 1450 6150 1000
Wire Wire Line
	6000 1450 6000 1550
Connection ~ 6000 1450
Wire Wire Line
	4150 1850 4550 1850
Wire Wire Line
	4250 2000 4250 1850
Connection ~ 4250 1850
Wire Wire Line
	4250 2300 4250 2400
Wire Wire Line
	4250 2400 4550 2400
Wire Wire Line
	4550 2400 4550 1950
Wire Wire Line
	4400 2400 4400 2500
Connection ~ 4400 2400
Wire Wire Line
	5750 1850 6150 1850
Wire Wire Line
	5850 2000 5850 1850
Connection ~ 5850 1850
Wire Wire Line
	5850 2300 5850 2400
Wire Wire Line
	5850 2400 6150 2400
Wire Wire Line
	6150 2400 6150 1950
Wire Wire Line
	6000 2400 6000 2500
Connection ~ 6000 2400
Wire Wire Line
	4150 2750 4550 2750
Wire Wire Line
	4250 2900 4250 2750
Connection ~ 4250 2750
Wire Wire Line
	4250 3200 4250 3300
Wire Wire Line
	4250 3300 4550 3300
Wire Wire Line
	4550 3300 4550 2850
Wire Wire Line
	4400 3300 4400 3400
Connection ~ 4400 3300
Wire Wire Line
	5750 2750 6150 2750
Wire Wire Line
	5850 2900 5850 2750
Connection ~ 5850 2750
Wire Wire Line
	5850 3200 5850 3300
Wire Wire Line
	5850 3300 6150 3300
Wire Wire Line
	6150 3300 6150 2850
Wire Wire Line
	6000 3300 6000 3400
Connection ~ 6000 3300
Wire Wire Line
	4150 3700 4550 3700
Wire Wire Line
	4250 3850 4250 3700
Connection ~ 4250 3700
Wire Wire Line
	4250 4150 4250 4250
Wire Wire Line
	4250 4250 4550 4250
Wire Wire Line
	4550 4250 4550 3800
Wire Wire Line
	4400 4250 4400 4350
Connection ~ 4400 4250
Wire Wire Line
	5750 3700 6150 3700
Wire Wire Line
	5850 3850 5850 3700
Connection ~ 5850 3700
Wire Wire Line
	5850 4150 5850 4250
Wire Wire Line
	5850 4250 6150 4250
Wire Wire Line
	6150 4250 6150 3800
Wire Wire Line
	6000 4250 6000 4350
Connection ~ 6000 4250
Wire Wire Line
	4150 4600 4550 4600
Wire Wire Line
	4250 4750 4250 4600
Connection ~ 4250 4600
Wire Wire Line
	4250 5050 4250 5150
Wire Wire Line
	4250 5150 4550 5150
Wire Wire Line
	4550 5150 4550 4700
Wire Wire Line
	4400 5150 4400 5250
Connection ~ 4400 5150
Wire Wire Line
	5750 4600 6150 4600
Wire Wire Line
	5850 4750 5850 4600
Connection ~ 5850 4600
Wire Wire Line
	5850 5050 5850 5150
Wire Wire Line
	5850 5150 6150 5150
Wire Wire Line
	6150 5150 6150 4700
Wire Wire Line
	6000 5150 6000 5250
Connection ~ 6000 5150
Wire Wire Line
	4150 5550 4550 5550
Wire Wire Line
	4250 5700 4250 5550
Connection ~ 4250 5550
Wire Wire Line
	4250 6000 4250 6100
Wire Wire Line
	4250 6100 4550 6100
Wire Wire Line
	4550 6100 4550 5650
Wire Wire Line
	4400 6100 4400 6200
Connection ~ 4400 6100
Wire Wire Line
	5750 5550 6150 5550
Wire Wire Line
	5850 5700 5850 5550
Connection ~ 5850 5550
Wire Wire Line
	5850 6000 5850 6100
Wire Wire Line
	5850 6100 6150 6100
Wire Wire Line
	6150 6100 6150 5650
Wire Wire Line
	6000 6100 6000 6200
Connection ~ 6000 6100
Wire Notes Line
	7100 700  7100 2450
Wire Notes Line
	7100 700  10750 700 
Wire Notes Line
	7100 2700 7100 5400
Wire Notes Line
	7100 5400 11100 5400
Wire Notes Line
	3700 700  3700 6350
Wire Notes Line
	3700 6350 6950 6350
Wire Notes Line
	6950 6350 6950 700 
Wire Notes Line
	6950 700  3700 700 
Wire Wire Line
	9650 4900 9650 4750
Connection ~ 9650 4750
Wire Notes Line
	3700 6550 3700 7500
Wire Notes Line
	6700 6550 6700 7500
Wire Wire Line
	7650 1950 7650 1800
Connection ~ 7650 1800
Wire Notes Line
	7100 2450 10750 2450
Wire Wire Line
	9600 2850 10450 2850
Connection ~ 9750 2850
Connection ~ 10000 2850
Connection ~ 10250 2850
Wire Notes Line
	11100 5400 11100 2700
Wire Wire Line
	2250 1600 2250 1700
Wire Wire Line
	1300 1700 2650 1700
Wire Wire Line
	2500 1600 2500 1700
Connection ~ 2500 1700
Wire Wire Line
	2400 2450 2400 2600
Wire Wire Line
	1300 2600 2500 2600
Connection ~ 2400 2600
Connection ~ 2250 1700
Wire Wire Line
	2450 3850 2600 3850
Wire Wire Line
	1150 5400 1250 5400
Wire Wire Line
	2450 5400 2550 5400
Wire Wire Line
	1150 3850 1250 3850
Wire Wire Line
	1150 6750 1250 6750
Wire Wire Line
	2450 6750 2550 6750
Wire Wire Line
	4400 1550 4600 1550
Wire Wire Line
	6000 1550 6200 1550
Wire Wire Line
	4400 2500 4600 2500
Wire Wire Line
	6000 2500 6200 2500
Wire Wire Line
	4400 3400 4600 3400
Wire Wire Line
	6000 3400 6200 3400
Wire Wire Line
	4400 4350 4600 4350
Wire Wire Line
	6000 4350 6200 4350
Wire Wire Line
	4400 5250 4600 5250
Wire Wire Line
	6000 5250 6200 5250
Wire Wire Line
	4400 6200 4600 6200
Wire Wire Line
	6000 6200 6200 6200
Wire Wire Line
	5900 7150 5900 7350
Wire Wire Line
	5900 7350 5950 7350
Wire Wire Line
	4450 7150 4450 7350
Wire Wire Line
	4450 7350 4500 7350
Wire Notes Line
	6700 7500 3700 7500
Wire Wire Line
	8450 900  8450 1400
Wire Wire Line
	7650 1400 8550 1400
Wire Wire Line
	7650 1350 7650 1400
Connection ~ 8450 1400
Wire Wire Line
	10100 900  10100 1350
Wire Wire Line
	9300 1350 10150 1350
Connection ~ 10100 1350
Wire Wire Line
	8450 1800 8450 2300
Wire Wire Line
	7650 2300 8550 2300
Wire Wire Line
	7650 2250 7650 2300
Connection ~ 8450 2300
Wire Notes Line
	10750 2450 10750 700 
Wire Wire Line
	7850 3150 7850 3400
Wire Wire Line
	7750 3400 9050 3400
Wire Wire Line
	8100 3400 8100 3150
Connection ~ 7850 3400
Wire Wire Line
	8350 3400 8350 3150
Connection ~ 8100 3400
Wire Wire Line
	9050 3400 9050 2850
Connection ~ 8350 3400
Wire Wire Line
	9750 3150 9750 3400
Wire Wire Line
	9650 3400 10950 3400
Wire Wire Line
	10000 3400 10000 3150
Connection ~ 9750 3400
Wire Wire Line
	10250 3400 10250 3150
Connection ~ 10000 3400
Wire Wire Line
	10950 3400 10950 2850
Connection ~ 10250 3400
Wire Wire Line
	7850 4200 7850 4350
Wire Wire Line
	7800 4350 8800 4350
Wire Wire Line
	8800 4350 8800 3750
Connection ~ 7850 4350
Wire Wire Line
	9600 4200 9600 4350
Wire Wire Line
	9550 4350 10550 4350
Wire Wire Line
	10550 4350 10550 3750
Connection ~ 9600 4350
Wire Wire Line
	9650 5200 9650 5250
Wire Wire Line
	9450 5250 10550 5250
Wire Wire Line
	10550 5250 10550 4750
Connection ~ 9650 5250
Wire Wire Line
	7850 5200 7850 5250
Wire Wire Line
	7800 5250 8800 5250
Wire Wire Line
	8800 5250 8800 4750
Connection ~ 7850 5250
Wire Wire Line
	2850 3650 2900 3650
Wire Wire Line
	2450 3650 2550 3650
Wire Wire Line
	2850 5200 2900 5200
Wire Wire Line
	2450 5200 2550 5200
Wire Wire Line
	2850 6550 2900 6550
Wire Wire Line
	2450 6550 2550 6550
Text GLabel 2550 6750 2    60   Output ~ 0
GND12V_1
Wire Notes Line
	3500 4300 3500 3000
Wire Notes Line
	3500 7150 3500 4550
Wire Notes Line
	3500 2750 3500 700 
Wire Wire Line
	8700 3700 8750 3700
Wire Wire Line
	8200 3700 8100 3700
Wire Wire Line
	10450 3700 10500 3700
Wire Wire Line
	9950 3700 9850 3700
$EndSCHEMATC
